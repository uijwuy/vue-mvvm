function defineReactive(obj, key, val) {

    observe(val) // 递归判断

    const dep = new Dep()
    Object.defineProperty(obj, key, {
        get() {
            // console.log('getter', val)
            Dep.target && dep.addDep(Dep.target)
            return val
        },
        set(nV) {
            if (val!=nV) {
                // console.log('setter', nV)
                observe(nV) // 赋值判断
                val = nV
                dep.notify()
            }
        }
    }) 
}

function observe(data) {
    if (typeof data !== 'object') {
        return data
    }
    Object.keys(data).forEach(key => defineReactive(data, key, data[key]))
}

function proxy(vm) {
    Object.keys(vm.$data).forEach(key => {
        Object.defineProperty(vm, key, {
            get() {
                return vm.$data[key]
            },
            set(nV) {
                vm.$data[key] = nV
            }
        })
    })
}

// vue 构造函数
class Vue {
    constructor(options) {
        this.$options = options
        this.$data = options.data

        proxy(this) // 把 data中的数据代理到vue实例上
        observe(this.$data) // 进行数据监听
        
        // 编译版本
        new Compile(this.$options.el, this)
    }
}

class Compile {
    constructor(el, vm) {
        this.$vm = vm // 保存vue实例

        // 编译模板
        this.compile(document.querySelector(el))
    }

    compile(el) {
        // 遍历el
        el.childNodes.forEach(node => {
            if (node.nodeType === 1) { //元素
                // 获取元素所有属性 
                Array.from(node.attributes).forEach(attr => {
                    if (attr.name == 'v-html') {
                        this.update(node, attr.value, 'Html')
                    }
                })

                // 元素需要递归
                if (node.childNodes.length > 0) {
                    this.compile(node)
                }
            } else { // 插值
                if (node.nodeType === 3 && /\{\{(.*)\}\}/.test(node.textContent)) {
                    // console.log(RegExp.$1) //插值的key
                    this.update(node, RegExp.$1, 'Text')
                }
            }
        })
    }
    update(node, key, type) {
        const fn = this['updater' + type]
        fn && fn(node, this.$vm[key])
        
        new Watcher(this.$vm, key, function(val) {
            fn && fn(node, val)
        })
    }
    updaterHtml(node, val) {
        node.innerHTML = val
    }
    updaterText(node, val) {
        node.textContent = val
    }
}


class Watcher {
    constructor(vm, key, updateCb) {
        this.vm = vm
        this.key = key
        this.updateCb = updateCb

        //实例化的时候去触发依赖收集 
        Dep.target = this
        vm[key]
        Dep.target = null
    }
    update() {
        this.updateCb.call(this.vm, this.vm[this.key])
    }
}
class Dep {
    constructor() {
        this.deps = []
    }
    addDep(dep) {
        this.deps.push(dep)
    }
    notify() {
        this.deps.forEach((dep) => dep.update())
    }
}